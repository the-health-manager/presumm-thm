FROM pytorch/pytorch:latest

COPY requirements.txt /src/requirements.txt
WORKDIR /src

RUN pip install --upgrade pip && pip install -r requirements.txt

COPY src /src

ENTRYPOINT ["gunicorn", "-b", "0.0.0.0:8080", "--preload", "-t", "120", "api:api"]

