import nltk
from pdfminer.high_level import extract_text

nltk.download('punkt')
filename = '1909.03186.pdf'
tokenize = True
if __name__ == '__main__':
    output_text = extract_text(f'../raw_data/{filename}').replace('\r', '').replace('\n', ' ')

    if tokenize:
        from nltk.tokenize import sent_tokenize, word_tokenize

        sent_list = sent_tokenize(output_text)
        output_text = " [CLS] [SEP] ".join(" ".join(word_tokenize(sentence)) for sentence in sent_list)

    with open(f"../raw_data/output_{filename}.txt", "w") as file:
        file.write(output_text)
