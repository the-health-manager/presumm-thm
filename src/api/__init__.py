import os
from tempfile import NamedTemporaryFile, TemporaryDirectory

import falcon
import nltk
from nltk.tokenize import sent_tokenize, word_tokenize

from models import data_loader


class PingResource:
    def on_get(self, req, resp):
        resp.media = {"status": "ok"}


class InvocationResource:
    def __init__(self, args, device, predictor, tokenizer):
        self.args = args
        self.device = device
        self.predictor = predictor
        self.tokenizer = tokenizer

    def on_post(self, req, resp):
        req_media = req.media
        text = req_media["text"]
        resp.media = {"summary": self.preprocess_and_summarize(text)}

    def preprocess_and_summarize(self, text):
        with NamedTemporaryFile(mode='w+') as input_file, TemporaryDirectory() as output_dir:
            sent_list = sent_tokenize(text.replace('\r', '').replace('\n', ' '))
            output_text = " [CLS] [SEP] ".join(" ".join(word_tokenize(sentence)) for sentence in sent_list)
            input_file.write(output_text)
            input_file.flush()

            self.args.text_src = input_file.name
            self.args.result_path = output_dir + "/output"
            test_iter = data_loader.load_text(self.args, self.args.text_src, self.args.text_tgt, self.device, self.tokenizer)
            self.predictor.translate(test_iter, -1)
            filename = output_dir + "/output.-1.candidate"
            with open(filename, "r") as result_file:
                return result_file.readline()


from api.model import args, device, predictor, tokenizer

nltk.download('punkt')

print("Building API...")
api = falcon.API()
api.add_route('/ping', PingResource())
api.add_route('/invocations', InvocationResource(args, device, predictor, tokenizer))
